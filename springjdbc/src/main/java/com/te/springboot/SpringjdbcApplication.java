package com.te.springboot;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.te.springboot.entity.Student;
import com.te.springboot.repository.StudentRepository;

@SpringBootApplication
public class SpringjdbcApplication {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		SpringApplication.run(SpringjdbcApplication.class, args);
		
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(StudentConfiguration.class);
		
		 StudentRepository studentRepository = applicationContext.getBean("studentrepo",StudentRepository.class);
		 studentRepository.createStudent();
		 System.out.println("Student table created..!!");
		 
		 int insertStudent = studentRepository.insertStudent(new Student(3,"Fayaz Shaikh","Associate Software Engineer",50000));
		 System.out.println(insertStudent+" rows affected..");
		 
		 int updateStudent = studentRepository.updateStudent(new Student(3,"Mohit Narkhede","Associate Software Engineer",80000));
		 System.err.println("student updated... "+updateStudent+" is affected");
		 Student student = new Student();
		 student.setId(3);
		 
		 int deleteStudent = studentRepository.deleteStudent(student);
		 System.out.println("delete student..... "+deleteStudent);
		 
		 int insert = studentRepository.insert(new Student(4,"Manish Kumar","Associate Software Developer",50000));
		 System.err.println(insert+" Record Inserted.....");
		 
		 Student student1 = studentRepository.getStudent(1);
		 System.err.println(student1);
		 
		 
		 List<Student> list = studentRepository.getAll();
		 list.stream().forEach(System.err::println);
		 
		 
		 for (Student student2 : list) {
			System.out.println(student2);
		}
		 
	}

}
