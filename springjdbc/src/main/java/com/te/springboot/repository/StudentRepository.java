package com.te.springboot.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.te.springboot.entity.Student;

public class StudentRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * this is for set JdbcTemplate object.
	 * @param jdbcTemplate
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		// System.err.println("jdbctemplate*******************" + jdbcTemplate);
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * This method is used for create a table
	 */
	public void createStudent() {
		String query = "create table student(id int primary key,name varchar(50),designation varchar(40),salary int)";
		jdbcTemplate.execute(query);
	}

	/**
	 * This method is used to insert a record
	 * @param student
	 */
	public int insertStudent(Student student) {
		String query = "insert into student values ('" + student.getId() + "','" + student.getName() + "','"
				+ student.getDesignation() + "','" + student.getSalary() + "')";
		return jdbcTemplate.update(query);

	}

	/**
	 * This method is used to insert a record
	 * @param student
	 */
	public int insert(Student student) {
		String query = "insert into student(id,name,designation,salary)values(?,?,?,?)";
		return jdbcTemplate.update(query, student.getId(), student.getName(), student.getDesignation(),
				student.getSalary());
	}

	/**
	 * This method is used to update record
	 * @param student
	 * @return
	 */
	public int updateStudent(Student student) {
		String query = "update student set name='" + student.getName() + "',salary='" + student.getSalary()
				+ "'where id= '" + student.getId() + "' ";
		return jdbcTemplate.update(query);
	}

	/**
	 * This method is used to delete record
	 * @param student
	 * @return
	 */
	public int deleteStudent(Student student) {
		String query = "delete from student where id='" + student.getId() + "'         ";
		return jdbcTemplate.update(query);
	}
	
	/**
	 * This method is used to get the student record
	 * @return student
	 */
	public Student getStudent(Integer id) {
		String query="select * from student where id=?";
		RowMapper<Student> rowMapper = new RowMapperImpl();
		return jdbcTemplate.queryForObject(query,rowMapper,id);
		
	}
	
	
	/**
	 * This method is used to fetch all the records
	 */
	public List<Student> getAll(){
		String query="select * from student";
		List<Student> listStudent = jdbcTemplate.query(query, new RowMapperImpl());
		return listStudent;
	}
	
	
	
	

}
